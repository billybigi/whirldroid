package com.gregdev.whirldroid.activity;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.WhirlpoolApi;
import com.gregdev.whirldroid.WhirlpoolApiException;
import com.gregdev.whirldroid.layout.SeparatedListAdapter;
import com.gregdev.whirldroid.model.Forum;
import com.gregdev.whirldroid.service.DatabaseHandler;

/**
 * Displays the latest Whirlpool forums in a nice list format
 */
public class ForumList extends SherlockListActivity implements OnQueryTextListener {

	private SeparatedListAdapter forum_adapter;
	private ArrayList<Forum> forum_list;
	private ProgressDialog progress_dialog;
	private RetrieveForumsTask task;
	private int list_position;
	private ListView forum_listview;

	/**
	 * Private class to retrieve forums in the background
	 */
	private class RetrieveForumsTask extends AsyncTask<String, Void, ArrayList<Forum>> {

		private boolean clear_cache = false;
		private String error_message = "";
		
		public RetrieveForumsTask(boolean clear_cache) {
			this.clear_cache = clear_cache;
		}

		@Override
		protected ArrayList<Forum> doInBackground(String... params) {
			if (clear_cache || Whirldroid.getApi().needToDownloadForums()) {
				ForumList.this.runOnUiThread(new Runnable() {
				    public void run() {
				    	try {
					    	progress_dialog = ProgressDialog.show(ForumList.this, "Just a sec...", "Loading forums...", true, true);
							progress_dialog.setOnCancelListener(new CancelTaskOnCancelListener(task));
				    	}
				    	catch (BadTokenException e) { }
				    }
				});
				try {
					Whirldroid.getApi().downloadForums();
				}
				catch (final WhirlpoolApiException e) {
					error_message = e.getMessage();
					return null;
				}
			}
			forum_list = Whirldroid.getApi().getForums();
			return forum_list;
		}

		@Override
		protected void onPostExecute(final ArrayList<Forum> result) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (progress_dialog != null) {
						try {
							progress_dialog.dismiss(); // hide the progress dialog
							progress_dialog = null;
						}
						catch (Exception e) { }
						
						if (result != null) {
							Toast.makeText(ForumList.this, "Forums refreshed", Toast.LENGTH_SHORT).show();
						}
					}
					if (result != null) {
						setForums(forum_list); // display the forums in the list
					}
					else {
						Toast.makeText(ForumList.this, error_message, Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	}

	/**
	 * A private class to format the whim list items
	 */
	public class ForumAdapter extends ArrayAdapter<Forum> {

		private ArrayList<Forum> forum_items;

		public ForumAdapter(Context context, int textViewResourceId, ArrayList<Forum> forum_items) {
			super(context, textViewResourceId, forum_items);
			this.forum_items = forum_items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Forum forum = forum_items.get(position);
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.list_row_single, null);
			}
			if (forum != null) {
				TextView tt = (TextView) v.findViewById(R.id.top_text);
				TextView bt = (TextView) v.findViewById(R.id.bottom_text);
				if (tt != null) {
					tt.setText(forum.getTitle());
				}
				if (bt != null){
					
					//Date date = forum.getDate();
					//String timeText = Whirldroid.getTimeSince(date);
					
					//bt.setText(timeText + " ago");
				}
			}
			return v;
		}

	}

	/**
	 * Cancels the fetching of forums if the back button is pressed
	 */
	private class CancelTaskOnCancelListener implements OnCancelListener {
		private AsyncTask<?, ?, ?> task;
		public CancelTaskOnCancelListener(AsyncTask<?, ?, ?> task) {
			this.task = task;
		}

		public void onCancel(DialogInterface dialog) {
			if (task != null) {
				task.cancel(true);
			}
		}
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whim_list);
		
		// save reference to listview
		forum_listview = (ListView) findViewById(android.R.id.list);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getForums(false);
		
		registerForContextMenu(getListView());
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// go to last list position
		forum_listview.setSelection(list_position);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// save list position
		list_position = forum_listview.getFirstVisiblePosition();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menu_info) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menu_info;
		int pos = info.position;
		
		Forum forum = (Forum) forum_adapter.getItem(pos);
		
		menu.setHeaderTitle(R.string.ctxmenu_forum);
		
		if (!forum.getSection().equals("Favourites")) {
			menu.add(Menu.NONE, 0, 0, "Add to favourites");
		}
		else {
			menu.add(Menu.NONE, 1, 0, "Remove from favourites");
		}
		
	}
	
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		int pos = info.position;
		
		Forum forum = (Forum) forum_adapter.getItem(pos);
		DatabaseHandler db = new DatabaseHandler(this);
		
		switch (item.getItemId()) {
			case 0: // add to favourites
				db.addFavouriteForum(forum);
				getForums(false);
				return true;
				
			case 1:
				db.removeFavouriteForum(forum);
				getForums(false);
				return true;
		}
		
		return false;
	}

	/**
	 * On clicking a forum, display a list of threads within that forum
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Forum forum = (Forum) forum_adapter.getItem(position);

		Intent forum_intent = new Intent(getApplicationContext(), ThreadList.class);
		
		// pass the forum ID and forum name to the new activity
		Bundle bundle = new Bundle();
		bundle.putInt("forum_id", forum.getId());
		bundle.putString("forum_name", forum.getTitle());
		forum_intent.putExtras(bundle);
		
		startActivity(forum_intent); // display the thread list
	}

	/**
	 * Displays a list of forums
	 * Creates a new thread to get data in the background
	 * @param clear_cache True to clear cached data and download from the server
	 */
	private void getForums(boolean clear_cache) {
		task = new RetrieveForumsTask(clear_cache); // start new thread to retrieve forums
		task.execute();
	}

	/**
	 * Loads the forums into the list
	 * @param forum_list A list of all forums
	 */
	private void setForums(ArrayList<Forum> forum_list) {
		if (forum_list == null || forum_list.size() == 0) { // no forums found
			return;
		}

		forum_adapter = new SeparatedListAdapter(this);
		
		// get favourite forums
		DatabaseHandler db = new DatabaseHandler(this);
		ArrayList<Forum> favourites = db.getFavouriteForums();
		
		if (favourites.size() > 0) {
			ForumAdapter fa = new ForumAdapter(this, android.R.layout.simple_list_item_1, favourites);
			forum_adapter.addSection("Favourites", fa);
		}
		
		// keep track of the current section (forums are divided into sections)
		String current_section = null;
		ArrayList<Forum> forums = new ArrayList<Forum>();
		
		for (Forum f : forum_list) {
			if (!f.getSection().equals(current_section)) { // we've reached a new section
				if (!forums.isEmpty()) { // there are items from a previous section
					ForumAdapter fa = new ForumAdapter(this, android.R.layout.simple_list_item_1, forums);
					forum_adapter.addSection(current_section, fa);
					//articles.clear();
					forums = new ArrayList<Forum>();
				}
				current_section = f.getSection();
			}
			forums.add(f);
		}
		// if we have forums to display
		if (!forums.isEmpty()) {
			ForumAdapter fa = new ForumAdapter(this, android.R.layout.simple_list_item_1, forums);
			forum_adapter.addSection(current_section, fa);
		}
		
		setListAdapter(forum_adapter); // display the forums
	}

	/**
	 * Create menu button items
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.refresh, menu);
		
		//boolean isLight = SampleList.THEME == R.style.Theme_Sherlock_Light;

		if (android.os.Build.VERSION.SDK_INT >= 8) {
	        //Create the search view
	        SearchView search_view = new SearchView(getSupportActionBar().getThemedContext());
	        search_view.setQueryHint("Search for threads…");
	        search_view.setOnQueryTextListener(this);
	
	        menu.add("Search")
	            .setIcon(R.drawable.abs__ic_search)
	            .setActionView(search_view)
	            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		}
		
		return true;
	}

	/**
	 * Menu button item selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_refresh:
				long now = System.currentTimeMillis() / 1000;
				// don't refresh too often
				if (now - Whirldroid.getApi().getForumsLastUpdated() > WhirlpoolApi.REFRESH_INTERVAL) {
					getForums(true);
				}
				else {
					Toast.makeText(ForumList.this, "Wait " + WhirlpoolApi.REFRESH_INTERVAL + " seconds before refreshing", Toast.LENGTH_LONG).show();
				}
				return true;

			case android.R.id.home:
				Intent dashboard_intent = new Intent(this, Dashboard.class);
				dashboard_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(dashboard_intent);
				return true;
		}
		return false;
	}

	public boolean onQueryTextChange(String new_text) {
		return false;
	}

	public boolean onQueryTextSubmit(String query) {
		Intent search_intent = new Intent(this, ThreadList.class);
		
		Bundle bundle = new Bundle();
		bundle.putInt("forum_id", WhirlpoolApi.SEARCH_RESULTS);
		bundle.putString("search_query", query);
		bundle.putInt("search_forum", -1);
		bundle.putInt("search_group", -1);
		
		search_intent.putExtras(bundle);
		
		startActivity(search_intent);
		
		return true;
	}
}